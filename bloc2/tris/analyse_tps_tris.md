**Analyse en temps de différentes méthodes de tris** 
-----


## Objectif de la séance:

- Analyser et comparer en temps les différentes méthodes de tris proposées, qu'elles aient été déjà vues/programmées ou non.

## Notions abordées et pré-requis:

- **Algorithmique**
	+ Ecrire un algorithme de tri

- **Pré-requis**
    + Notions de tris
	+ Programmation de certains tris (optionnel)

- **Compétences Transversales** :
    + Analyse de documents (graphiques, programmes)
    + Justifier ses choix

- **Notions vues lors d'un cours précédent mais non utilisées**
    + Connaissance des tris utilisés lors de l'activité

## Organisation de la séance

Activité débranchée sous forme d'analyse de documents principalement. 
Cette séance fait suite à une première séance où les élèves auront manipulé physiquement des objets (masses, bâtons, ...) 
qu'ils auront dû trier avec une méthode en commençant par celles qu'ils proposeront

__Exemple de protocole__ : même méthode par groupe d'élèves avec possibilité de chronométrer puis on garde le meilleur temps ou la moyenne des meilleurs temps et on change de méthode de tri.

## Préparation de l'activité en amont 

Il faut avoir programmé les différentes méthodes de tris: insertion et sélection (par les élèves), fusion, rapide et .sort()) et avoir obtenu un graphique permettant d'afficher les différentes cours en temps, selon la liste initiale et la longueur de cette liste.

### Exemples de courbes de temps de différentes méthodes de tris lorsque la liste initiale est triée de manière croissante

|Listes de taille 40|Listes de taille 80|Listes de taille 100|
|:-------------:|:-------------:|:-------------:|
|![Comp tris liste croissante de taille 40](./courbes/croissante_40.png)|![Comp tris liste croissante de taille 80](./courbes/croissante_80.png)|![Comp tris liste croissante de taille 100](./courbes/croissante_100.png)|


1. En comparant les différentes courbes, y a-t-il une méthode de tri plus performante que les autres?
2. A l'exception de la méthode `sort`, quelles sont les méthodes de tris la plus performante et la moins performante? _(Il est sera judicieux de distinguer plusieurs cas.)_ 

### Exemples de courbes de temps de différentes méthodes de tris lorsque la liste initiale est triée de manière décroissante

|Listes de taille 40|Listes de taille 80|Listes de taille 100|
|:-------------:|:-------------:|:-------------:|
|![Comp tris liste décroissante de taille 40](./courbes/decroissante_40.png)|![Comp tris liste décroissante de taille 80](./courbes/decroissante_80.png)|![Comp tris liste décroissante de taille 100](./courbes/decroissante_100.png)|


3. En comparant les différentes courbes, y a-t-il une méthode de tri plus performante que les autres?
4. A l'exception de la méthode `sort`, quelles sont les méthodes de tris la plus performante et la moins performante? _(Il est sera judicieux de distinguer plusieurs cas.)_ 
5. En comparant avec les réponses obtenues pour une liste initiale triée de manière croissante, que constate-t-on?

### Exemples de courbes de temps de différentes méthodes de tris lorsque la liste initiale est mélangée

|Listes de taille 40|Listes de taille 80|Listes de taille 100|
|:-------------:|:-------------:|:-------------:|
|![Comp tris liste mélangée de taille 40](./courbes/melangee_40.png)|![Comp tris liste mélangée de taille 80](./courbes/melangee_80.png)|![Comp tris liste mélangée de taille 100](./courbes/melangee_100.png)|


6. Mêmes questions que précédemment.

### Conclusion

7. En tenant compte des différentes analyses effectuées précédemment, que peut-on en conclure?

### Et si on voyait plus grand...?

|Liste initiale croissante de taille 150|Liste initiale décroissante de taille 80|Liste initiale mélangée de taille 100|
|:-------------:|:-------------:|:-------------:|
|![Comp tris liste croissante de taille 150](./courbes/croissante_150.png)|![Comp tris liste décroissante de taille 150](./courbes/decroissante_150.png)|![Comp tris liste mélangée de taille 150](./courbes/melangee_150.png)|

8. En observant les graphiques ci-dessus, que constatez-vous?

## Méthode `sort`
-----
9. En comparant les courbes obtenues avec celles obtenues pour les quatre méthodes de tris étudiées, le(s)quel(s) pourrai(en)t être celui implantant la méthode `sort` ?

	+ Cette méthode est bien plus rapide que toutes les autres et de loin. 
	+ Elle semble avoir une progression linéaire dans le temps. 
	+ On remarque quelques similitudes avec certaines méthodes de tri:
		- liste initiale triée de manière croissante: tri par insertion
		- liste initiale triée de manière décroissante: tri fusion (pour des listes de grande taille)
		- liste initiale mélangée: tri rapide


## Prolongements/améliorations possibles:
+ Rattacher cette activité à quelque chose de concret:
    - classement de joueurs, de résultats,...