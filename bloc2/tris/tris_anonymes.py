#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

:author: DIU-Bloc2 - Univ. Lille
:date: 2019, mai

Tris de listes

"""

import random

def compare_entier_croissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int)
             * >0  si a est supérieur à b
             * 0 si a est égal à b
             * <0 si a est inférieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_croissant(1, 3) < 0
    True
    """
    return a-b

def compare_entier_décroissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int)
             * >0 si a est inférieur à b
             * 0 si a est égal à b
             * <0 si a est supérieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_décroissant(1, 3) > 0
    True
    """
    return b-a

def est_trie(l, comp):
    """
    :param l: (type sequentiel) une séquence
    :param comp: une fonction de comparaison
    :return: (bool)
      - True si l est triée
      - False sinon
    :CU: les éléments de l doivent être comparables
    :Exemples:

    >>> est_trie([1, 2, 3, 4], compare_entier_croissant)
    True
    >>> est_trie([1, 2, 4, 3], compare_entier_croissant)
    False
    >>> est_trie([], compare_entier_croissant)
    True
    """
    i = 0
    res = True
    while res and i < len(l) - 1:
        res = comp(l[i], l[i+1]) <= 0
        i += 1
    return res

def cree_liste_melangee(n):
    """
    :param n: (int) Longueur de la liste à créer
    :return: une permutation des n premiers entiers
    :CU: n >= 0
    :Exemples:

    >>> l = cree_liste_melangee(5)
    >>> len(l)
    5
    >>> sorted(l) == [0, 1, 2, 3, 4]
    True
    """
    l = list(range(n))
    random.shuffle(l)
    return l


################################################
#                  TRI 1                       #
################################################


def tri_1(l, comp):
    """
    Tri aléatoire
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables

    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre
        défini par comp, du plus petit au plus grand
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_1(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    while not est_trie(l, comp):
        i = random.randint(0,len(l)-2)
        if comp(l[i], l[i+1]) > 0:
            tmp = l[i]
            l[i] = l[i+1]
            l[i+1] = tmp

################################################
#                   TRI 2                      #
################################################

def tri_2(l, comp):
    """
    Tri par insertion
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables

    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre
        défini par comp, du plus petit au plus grand
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_2(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
        l[k] = tmp


################################################
#                   TRI 3                      #
################################################

def fais_quelque_chose(l, comp, d, f):
    p = l[d]
    ip = d
    for i in range (d+1, f):
        if comp(p, l[i]) > 0:
            l[ip] = l[i]
            l[i] = l[ip+1]
            ip = ip + 1
    l[ip] = p
    return ip

def tri_3(l, comp, d=0, f=None):
    """
    Tri rapide (optimisé car le pivot est choisi d'une certaine manière à avoir plus petit avant et plus grand après)
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :param d: un entier >= 0 et < len(l) (0 par défaut)
    :param f: un entier <= len(l) ou None. None équivaut à len(l)
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables

    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre
        défini par comp, du plus petit au plus grand
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_3(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    if f is None: f = len(l)
    if f - d > 1:
        ip = fais_quelque_chose(l, comp, d, f)
        tri_3(l, comp, d=d, f=ip)
        tri_3(l, comp, d=ip+1, f=f)



##def tri_selection(l,comp):
##    """
##    :param l: (list) une liste
##    :param comp: une fonction de comparaison
##    :CU: Les éléments de l doivent être comparables
##
##    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre
##        défini par comp, du plus petit au plus grand
##    :Exemples:
##
##    >>> l = [3, 1, 4, 1, 5, 9, 2]
##    >>> tri_selection(l, compare_entier_croissant)
##    >>> l == [1, 1, 2, 3, 4, 5, 9]
##    True
##    """



def compare_chaine_lexicographique(l,m):
    """
    :param l: (liste) une chaine
    :param m: (liste) une chaine
    :return: (liste)
             * ==0 si m est supérieur à l
             * !=0 si m est inférieur à l
    :CU: aucune
    :Exemples:

    >>> compare_chaine_lexicographique('a','bc') < 0
    True
    """
    return l>m


def compare_chaine_longueur(l,m):
    """
    :param l: (liste) une chaine
    :param m: (liste) une chaine
    :return: (liste)
             * >0 si len(m) est inférieur à len(l)
             * 0 si len(m) est égal à len(l)
             * <0 si len(m) est supérieur à len(l)
    :CU: aucune
    :Exemples:

    >>> compare_chaine_longueur('a','abc')==0
    True
    """
    return len(l)-len(m)


def fusion(l1,l2):
    if l1==[]:
        return l2
    if l2==[]:
        return l1
    for i in range(len(l1)):
        if l1[i]<l2[i]:
            return [l1[i]]+fusion(l1[i+1 :],l2)
        else:
            return [l2[i]]+fusion(l1,l2[i+1 :])

def tri_fusion(l):
    """
    :param l: (liste) un entier
    :CU:aucune
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_fusion(l)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    if len(l)<=1:
        return l
    for i in range(len(l)//2):
        l1=[l[i]]
        print(l1)
    for j in range(len(l)//2,len(l)):
        l2=[l[j]]
        print(l2)
    return fusion(l1,l2)
