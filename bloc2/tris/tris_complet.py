from listes import *
from tris import *
import pylab

from timeit import timeit

    
def calcul_tps(tri,liste,l):
    '''
    :param tri: (str) tri choisi
    :param l: (int) longueur d'une liste donnée
    :return: (liste) liste de mesure de tps pour des listes de longueur croissante jusqu'à l
    :CU: aucune
    '''
    if tri == "sort" :
        return timeit(setup='from listes import cree_liste_{}'.format(liste),
                       stmt='list(cree_liste_{}({})).sort()'.format(liste,l),
                       number=100)
    else:
        return timeit(setup='from tris import {}; from listes import cree_liste_{}'.format(tri,liste),
               stmt= '{}(cree_liste_{}({}))'.format(tri,liste,l),
               number=100)



def calcul_tps_total(tri,liste,l):
    '''
    :param l: (int) longueur d'une liste donnée
    :return: (liste) liste de mesure de tps pour des listes de longueur croissante jusqu'à l
    :CU: aucune
    '''
    temps=[]
    for i in range(1,l+1):
        m=calcul_tps(tri,liste,i)
        temps.append(m)
    return temps



def construction_courbe_tri(tri,liste,l):
    '''
    :param tri: (str) méthode de tri utilisée
    :param liste: (str) méthode de création de liste utilisée
    :param l: (int) longueur d'une liste donnée
    :return: None
    :CU: aucune
    '''
    x=[j for j in range(1,l+1)]
    y=calcul_tps_total(tri,liste,l)
    
    NBRE_ESSAIS = 100
    pylab.title('Temps du tri avec une liste initiale {} (pour {:d} essais)'.format(liste,NBRE_ESSAIS))
    pylab.legend()    
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.plot(x,y,label='{}'.format(tri))

#    pylab.show()
    
    
def affichage_multiple(liste,l):
    '''
    :param liste: (str) méthode de création de liste utilisée
    :param l: (int) longueur d'une liste donnée
    :return: courbes associées aux différentes méthodes de tri
            selon la méthode de création de liste utilisée
    :CU: aucune
    '''
    tris = ['tri_select', 'tri_insert', 'tri_fusion', 'tri_rapide','sort', ]
    for tri in tris:
        construction_courbe_tri(tri,liste,l)
        pylab.legend()
    pylab.show()

if __name__ == '__main__':

    affichage_multiple('melangee',150) #Exemple de courbes