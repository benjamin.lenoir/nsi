import TSP_biblio as tsp
import copy

def matrice_distance(fichier):
    """
    : lecture fichier de ville format ville, latitude, longitude
    : renvoie une matrice contenant les distances 2 à 2 entre toutes les villes
    : param fichier: file
    : return : (list)
    """
    try:
        liste=tsp.get_tour_fichier('{}'.format(fichier))
        nbre_ville=len(liste)
        dist=[]
        
        for i in range(nbre_ville):
            dist2=[]
            
            for j in range(nbre_ville):
                dist2.append( tsp.distance(liste,i,j) )
                
            dist.append(dist2)
            
    except FileNotFoundError:
        return 'Le fichier n\' existe pas'
    
    return dist




def liste_villes(fichier):
    """
    : lecture fichier de ville format ville, latitude, longitude
    : renvoie une liste contenant les noms des villes
    : param fichier: file
    : return : (list) une liste de villes
    """
    liste = tsp.get_tour_fichier('{}'.format(fichier))
    villes = []
    
    for c in liste:
        villes.append( c[0] )
    
    return villes

       
def indice_ville_proche(ville,liste_ville,dist):
    """
    : renvoie l'indice de la ville la plus proche
    : param ville: (int) indice d'une ville quelconque
    : param liste_ville: (list) la liste d'indice des villes
    : param dist: une matrice contenant les distances 2 à 2 pour toutes les villes
    : return : (int) indice de la ville la plus proche de la ville de départ
    """
    minimum = float('inf')
    
    for indice_ville in liste_ville :
        distance = dist[ville][indice_ville]
        
        if distance < minimum :
            minimum = distance
            indice_min = indice_ville
    return indice_min



def chemin_villes(ville, liste_villes , dist):
    '''
    : renvoie la liste des villes à visiter selon la ville de départ sous forme d'indice
    : param ville: (str) ville de départ
    : param liste_villes: (list) liste des villes
    : param dist: (list) une matrice contenant les distances 2 à 2 pour toutes les villes
    : return : (list) liste des villes dans l'ordre de parcours
    '''

    chemin = []
    liste2 = [_ for _ in range(len(liste_villes))]
    indice_ville = liste_villes.index(ville)
    
    if indice_ville in liste_villes:
        liste_villes.remove(indice_ville)
    
    while len(liste2)>0:
        indice_ville_suivante = indice_ville_proche(indice_ville, liste2, dist)
        chemin.append(indice_ville_suivante)
        ville_suivante = liste_villes[indice_ville_suivante]     
        liste2.remove(indice_ville_suivante)
        ville = ville_suivante
        indice_ville = indice_ville_suivante
    
    return chemin


def affichage_tour(liste_villes):
    '''
    : renvoie la liste des villes à visiter selon la ville de départ
    : param liste_villes: (list) liste des villes
    : return : (list) liste des villes dans l'ordre de parcours avec leurs coordonnées
    '''
    liste=tsp.get_tour_fichier('exemple.txt')
    parcours=[]
    
    for ville in liste_villes:
        parcours.append(liste[ville])
        
    return parcours
        

#def parcours_minimal(parcours):
#    '''
#    : renvoie la liste des villes dont le parcours est minimal
#    : param parcours: (list) liste de villes avec leurs coordonnées
#    : return : (list) liste des villes dans l'ordre de parcours
#    '''



if __name__ == '__main__':
    import doctest
    doctest.testmod()

    
dist=matrice_distance('exemple.txt')
villes=liste_villes('exemple.txt')
chemin=chemin_villes('Caen',villes,dist)
parcours=affichage_tour(chemin)
tsp.trace(parcours)

