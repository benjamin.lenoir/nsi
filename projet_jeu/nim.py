

def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    '''
#    try:
    nb_cailloux = 20
    print('Il y a {} cailloux'.format(nb_cailloux))
#        res = int(input())
#        assert 1 < res < 4
#        
#    except AssertionError:
#        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3')
#        return situation_initiale()
#    
#    except ValueError:
#        print('Veuillez saisir un nombre')
#        return situation_initiale()
#        
    return nb_cailloux


def situation_courante(nb_cailloux):
    '''
    Donne la situation du jeu/plateau à un instant t
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    print('Il reste {} cailloux'.format(nb_cailloux))
    


def game_over(nb_cailloux,joueur):
    '''
    Détermine la fin d'une partie avec affichage du résultat
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ param joueur: (str) type de joueur (humain ou machine)
    '''
    
    if nb_cailloux == 0:
        print('Le joueur {} a perdu'.format(joueur))
    elif nb_cailloux == 1:
        print('Le joueur {} a gagné'.format(joueur))
    else:
        print('Les programmeurs ont merdé quelque part... ^^')



    
def choix_humain(nb_cailloux):
    '''
    Action du joueur humain selon les possibilités
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    try:
        print('Combien prenez-vous de cailloux ?'.format(nb_cailloux))
        res = int(input())
        print('')
        assert 1 < res < 4
        
    except AssertionError:
        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3. ')
        return choix_humain(nb_cailloux)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(nb_cailloux)
    
    return nb_cailloux-res



def choix_machine(nb_cailloux):
    '''
    Action du joueur machine selon l'algorithme voulu (ici min_max)
    '''
    if nb_cailloux % 5 == 4:
        res = 3
    else:
        res = 2
    print('Le joueur machine a retiré {} cailloux'.format(res))
    print('')
    return nb_cailloux-res



def test_poursuite(nb_cailloux):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    '''
    
    if nb_cailloux >= 2:
        return True
    else:
        return False
