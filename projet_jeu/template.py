

def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    '''
    pass

    

def situation_courante():
    '''
    Donne la situation du jeu/plateau à un instant t
    '''
    pass


def game_over():
    '''
    Détermine la fin d'une partie avec affichage du résultat
    '''
    pass


def choix_humain():
    '''
    Action du joueur humain selon les possibilités
    '''
    pass


def choix_machine():
    '''
    Action du joueur machine selon l'algorithme voulu (ici min_max)
    '''
    pass


def test_poursuite():
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    '''
    pass
