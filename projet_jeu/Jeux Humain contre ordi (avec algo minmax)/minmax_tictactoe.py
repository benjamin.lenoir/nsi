### Création et affichage du plateau de départ ou en cours de jeu

from copy import deepcopy

def hauteur_grille(grille):
    return len(grille)


def largeur_grille(grille):
    for i in range(len(grille)):
        return len(grille[i])### Création et affichage du plateau de départ ou en cours de jeu


def hauteur_grille(grille):
    return len(grille)


def largeur_grille(grille):
    for i in range(len(grille)):
        return len(grille[i])


def affichage(grille):
    '''
    Affiche sous forme de grille la liste entrée en paramètre
    @ param grille: (list) une grille
    '''
    L = [[3,grille[0][0],grille[0][1],grille[0][2]],
        [2,grille[1][0],grille[1][1],grille[1][2]],
        [1,grille[2][0],grille[2][1],grille[2][2]],
        [4,1,2,3]]
    temp=''
    m=''
    for i in range(largeur_grille(L)):
        for j in range(hauteur_grille(L)):
            if L[i][j]==0:
                temp='|_|'
            elif L[i][j]=='X':
                temp='|X|'
            elif L[i][j]=='O':
                temp='|O|'
            elif L[i][j]==1:
                temp=' 1 '
            elif L[i][j]==2:
                temp=' 2 '
            elif L[i][j]==3:
                temp=' 3 '
            elif L[i][j]==4:
                temp='   '
            m=m+temp
        print(m,end='\n')
        m=''


def nb_elements(grille):
    '''
    Retourne le nombre de cases occupées et de cases vides
    @ param grille: (list) une grille
    return: (int) nbre de cases occupées et vides
    '''

    nb_occupees = 0
    nb_vides = 0
    for j in range(largeur_grille(grille)):
        for i in range(hauteur_grille(grille)):
            if grille[i][j] == 0:
                nb_vides+=1
    nb_occupees = largeur_grille(grille)*hauteur_grille(grille) - nb_vides

    return nb_occupees, nb_vides


def cases_restantes(grille):
    '''
    Retourne la liste des coordonnées des cases_restantes
    @ param grille: (list) une grille
    return: liste de couples (i,j)
    '''

    cases_restantes = []

    for j in range(largeur_grille(grille)):
        for i in range(hauteur_grille(grille)):
            if grille[j][i] == 0:
                cases_restantes.append((i,j))

    return cases_restantes

### Tic-tac-toe

def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    return: (list) la grille de départ créée selon les dimensions voulues
    '''
    grille=[[0,0,0],[0,0,0],[0,0,0]]
    affichage(grille)

    return grille



def situation_courante(grille):
    '''
    Donne la situation du jeu/plateau à un instant t
    @ param grille: (list) grille du tic-tac-toe actuelle
    '''
    affichage(grille)




def game_over(grille,joueur):
    '''
    Détermine la fin d'une partie avec affichage du résultat
    @ param grille: grille de la partie en cours à l'instant t
    @ param joueur: (str) type de joueur (humain ou machine)
    '''
    if test_poursuite(grille) == False:
        if nb_elements(grille)[0] != 9:
            print('Le joueur {} a gagné'.format(joueur))
        else:
            print('Match nul!')

    else:
        print('Les programmeurs ont merdé quelque part... ^^')




def choix_humain(grille):
    '''
    Action du joueur humain selon les possibilités
    @ param : grille du tic-tac-toe avant choix du joueur
    @ return: la grille modifiée selon le choix du joueur humain
    '''
    try:
        print('où voulez-vous jouer?')
        abscisse = int(input('abscisse = '))
        ordonnee = int(input('ordonnée = '))
        print('')
        assert 1 <= abscisse <= 3 and 1 <= ordonnee <= 3 and grille[3-ordonnee][abscisse-1] == 0
        grille[3-ordonnee][abscisse-1] = 'X'

    except AssertionError:
        print('Cette case n\'existe pas ou est déja occupée. ')
        return choix_humain(grille)

    except IndexError:
        print('Cette case n\'existe pas. ')
        return choix_humain(grille)

    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(grille)

    return grille


def test_poursuite(grille):
    '''
    Vérifie si la partie peut continuer à jouer
    @param : grille de TicTacToe à tester 
    @return: True si la partie peut se poursuivre
             False si la partie ne peut pas se poursuivre
    '''
    res = True
    for i in range(largeur_grille(grille)):
        if grille[i][0] == grille[i][1] == grille[i][2] == 'X' or grille[i][0] == grille[i][1] == grille[i][2] == 'O':
            res = False

    for j in range(hauteur_grille(grille)):
        if grille[0][j] == grille[1][j] == grille[2][j] == 'X' or grille[0][j] == grille[1][j] == grille[2][j] == 'O':
            res = False

    if (grille[0][0] == grille[1][1] == grille[2][2] == 'X' or
        grille[0][2] == grille[1][1] == grille[2][0] == 'X' or
        grille[0][0] == grille[1][1] == grille[2][2] == 'O' or
        grille[0][2] == grille[1][1] == grille[2][0] == 'O'):
        res = False
    
    if nb_elements(grille)[1] == 0 :
        res = False

    return res
    
def test_finalite(situation):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    '''
    if test_poursuite(situation):
        return False
    else:
        return True



def evaluation(situation):
    '''
    donne une note à la situation suivant le joueur
    1 si gagné
    0.5 si match nul
    0 si le jeu se poursuit
    '''
    if test_finalite(situation) == True :
        if nb_elements(situation)[1] == 0 :
            return 0.5
        else :
            return 1
    else :
        return 0

    
def situ_suiv(situation,joueur):
    '''
    @param : situation est la situation en cours
    @param : joueur est le joueur dont c'est le tour
    @return : renvoie une liste de toutes les situations suivantes  à celle rentrée en paramètre
    '''
    if joueur == 'machine':
        symbole = 'O'
    else :
        symbole = 'X'
    situ_suivantes = []
    for coord in cases_restantes(situation):
        copie_situation = deepcopy(situation)
        x,y = coord
        copie_situation[y][x] = symbole
        situ_suivantes.append(copie_situation)
    if situ_suivantes == []:
        situ_suivantes = deepcopy(situation)
    return situ_suivantes