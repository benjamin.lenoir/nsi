def changement_joueur(joueur):
    '''
    Permet l'alternance du joueur
    @ param joueur: (str) type de joueur (humain ou machine)
    @ return: change le joueur et renvoie humain si erreur d'argument
    '''
    if joueur == 'humain':
        joueur = 'machine'
    else:
        joueur = 'humain'
    return joueur

def coeff(joueur):
    '''
    @return : renvoie 1 si il s'agit du joueur actif et -1 sinon
    '''
    if joueur == 'machine' :
        return 1
    else :
        return -1
    

def min_max(situation, profondeur, joueur):
    '''
    algorithme classique min_max
    @ param : situation est la situation à évaluer par l algo min_max
    @ param : profondeur est le nombre de coups à l avance regardés
    @ param : joueur est le joueur actif dont on évalue la situation
    @ return : renvoie le score optimal obtenu pour le joueur avec la situation donnée et la profondeur choisie ainsi que la configuration
                permettant de l'obtenir
    '''
    if jeu.test_finalite(situation):
        return jeu.evaluation(situation)*coeff(joueur),jeu.situ_suiv(situation,joueur)[0]
    elif profondeur == 0 :
        return 0,jeu.situ_suiv(situation,joueur)[0]
    else :
        situations_suivantes = jeu.situ_suiv(situation,joueur)
        if joueur == 'machine' :
            L = [ min_max(situ, profondeur-1, 'humain')[0] for situ in situations_suivantes ]
            score = min(L)
        else :
            L = [ min_max(situ, profondeur-1, 'machine')[0] for situ in situations_suivantes ] 
            score = max(L)
        situ_optimale = situations_suivantes[L.index(score)]  
        return score,situ_optimale
        

def jouons():
    print('')
    print('Quelle difficulté voulez-vous ?')
    difficulte = int(input('Saisissez un nombre entre 1 et 10 : '))
    print('')
    print('Quel joueur commence ?')
    print('1. Vous')
    print('2. L\'ordinateur')
    choix_joueur = int(input('Saisissez 1 ou 2 : '))
    if choix_joueur == 1:
        joueur = 'machine'
    else :
        joueur = 'humain'
    situation = jeu.situation_initiale()
    
    while jeu.test_poursuite(situation):
        joueur = changement_joueur(joueur)
        
        if joueur == 'humain':
            situation = jeu.choix_humain(situation)
        else:
            print('\n')
            print('le joueur machine vient de jouer')
            situation = min_max(situation,difficulte,'machine')[1]
        jeu.situation_courante(situation)
    
    jeu.game_over(situation,joueur)
 

if __name__ == '__main__':
    print('Etes-vous prêt à jouer ?')
    print('')
    print('A quel jeu voulez-vous jouer ?')
    print('1. Le jeu de Nim')
    print('2. Le TicTacToe')
    choix_jeu = int(input('Saisissez 1 ou 2 : '))
    if choix_jeu == 1:
        import minmax_nim as jeu
        jouons()
    else :
        import minmax_tictactoe as jeu
        jouons()
