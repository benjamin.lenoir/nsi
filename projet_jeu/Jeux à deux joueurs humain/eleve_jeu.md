---
title: Fiche élève pour l'implémentation d'un jeu
subtitle : Bloc 1 & 2
author:  B. Lenoir et S. Malicet 
date: Juillet 2019
---



**Implémentation du Tic Tac Toe** 
-----


## Objectif de la séance:

- Implémentation d'un jeu (ici Tic Tac Toe)

## Notions abordées et pré-requis:

- **Représentation des données**
	+ Lire et modifier les éléments d’un tableau grâce à leurs index.
	+ Utiliser des tableaux de tableaux pour représenter des matrices : notation a[i][j].
	+ Itérer sur les éléments d’un tableau.

- **Langage et programmation**
	+ Mettre en évidence un corpus conditionnelles, boucles bornées, de constructions élémentaires.
	+ Utiliser la documentation d’une bibliothèque.


## Organisation de la séance

L'activité est guidée au début puis de moins en moins étant donné que ce jeu intervient après le jeu de Nim dans la progression.

## Préparation de l'activité en amont 

Cette activité fait suite à une ou plusieurs séances ayant eu pour but de construire le jeu de Nim avec les élèves, en faisant apparaître le schéma de ce type de jeu à deux joueurs.

En reprenant la structure des jeux à deux joueurs, nous allons créer deux fichiers:
 + une __interface__ qui servira à tout lancement de jeu à deux joueurs tels que nous les avons déjà vus;
 + un __template__ qui servira de base à toute nouvelle implémentation d'un jeu.


## Création des fichiers `template` et `interface`
-----
1. Créez un fichier `template.py` qui devra contenir les fonctions suivantes dont vous renseignerez la documentation:
	- `situation_initiale()`
	- `situation_courante()`
	- `game_over()`
	- `choix_humain()`
	- `choix_machine()`
	- `test_poursuite()`


**Début du fichier:**
```python
def situation_initiale():
    '''
    Partie à compléter par vos soins
    '''
    pass


def situation_courante():
    '''
    Partie à compléter par vos soins
    '''
    pass

```

2. Créez maintenant un fichier `interface.py`. Dans celui-ci, créez la fonction `changement_joueur()` qui prend en paramètre le joueur courant et alterne avec le joueur suivant:
	```python
	import tictactoe as jeu

	def changement_joueur(joueur):
	    '''
	    Permet l'alternance du joueur
	    @ param joueur: (str) type de joueur (humain ou machine)
	    @ return: change le joueur et renvoie humain si erreur d'argument
	    '''


	```
3. Nous allons maintenant créer "l'interface" que les joueurs auront sur leur écran. Pour cela, créons la fonction `jouons()` suivante:

	```python 
	def jouons():
	    '''
	    Lance le jeu importé
	    '''
	    print('Que la partie commence !\n \n')
	    joueur = 'humain'
	```
	+ Voici la structure de l'interface...mais elle est incomplète! 
	
	```python 
	    nom_des_joueurs = [__?????__,__?????__]
	    nombre_piece = jeu.__?????__()
	    nombre_piece = jeu.__?????__(nombre_piece,nom_des_joueurs)
	    jeu.__?????__(nombre_piece)
	    
	    while jeu.__?????__(nombre_piece):
		joueur = __?????__(joueur)
		
		if joueur == '__?????__':
		    nombre_piece = jeu.__?????__(nombre_piece,nom_des_joueurs)
		else:
		    nombre_piece = jeu.__?????__(nombre_piece,nom_des_joueurs)
		jeu.__?????__(nombre_piece)
	    
	    jeu.__?????__(nombre_piece,joueur,nom_des_joueurs)
	```
   + Copiez le code dans votre fichier.
   + En vous servant par exemple de votre fichier `template.py`, essayez de compléter les ????? de cette fonction.


## Implémentation du Tic-Tac-Toe
-----

#### Affichage du jeu

4. Créez un dossier `jeu_2_joueurs`, copiez le fichier `template.py` dedans et renommez le `tictactoe.py`.

Nous allons maintenant travailler uniquement dans le fichier `tictactoe.py`

5. Il nous faut une grille pour pouvoir représenter le jeu visuellement.
  
  + Créez une fonction `creer_grille()` qui prend en paramètre des dimensions pour une grille et retourne une grille aux dimensions voulues remplie avec des 0:
	```python
	def creer_grille(horizontal,vertical):
	    '''
	    @ param horizontal: (int) largeur de la grille voulue
	    @ param vertical: (int) hauteur de la grille voulue
	    return: (list) grille créée selon les dimensions
	    Exemple:
	    >>> creer_grille(3,3)
	    [[0,0,0],[0,0,0],[0,0,0]]	
	
	```

  + Créez deux fonctions `hauteur_grille()` et `largeur_grille()` qui prennent en paramètre une grille quelconque et qui renvoient ses dimensions.

6. Pour l'instant, nous ne "voyons" pas cette grille or nous en aurons besoin au cours du jeu d'où l'utilité d'une fonction `affichage()` qui prendra en paramètre la grille et l'affichera sur la console. A vous de jouer!

7. A partir de là, vous êtes capables de compléter les fonctions `situation_initiale()` et `situation_courante()`.


#### Mécanique du jeu

8. Il est temps maintenant de s'occuper de la partie mécanique du jeu:
  
  + Copiez le code dans votre fichier et complétez les ????? de cette fonction.
	```python
	def choix_humain(grille,nom_des_joueurs):
	    '''
	    Action du joueur humain selon les possibilités
	    @ param grille: (list) grille du tic-tac-toe avant choix du joueur
	    return: (list) la grille modifiée selon le choix du joueur humain
	    '''
	    try:
		print('{}, où voulez-vous jouer?'.format(nom_des_joueurs[0]))
		abscisse = __?????__
		ordonnee = __?????__
		print('')
		assert __?????__ and __?????__ and __?????__
		__?????__ = 'X'

	    except AssertionError:
		print('Cette case n\'existe pas ou est déja occupée. ')
		return choix_humain(grille,nom_des_joueurs)

	    except IndexError:
		print('Cette case n\'existe pas. ')
		return choix_humain(grille,nom_des_joueurs)

	    except ValueError:
		print('Veuillez saisir un nombre')
		return choix_humain(grille,nom_des_joueurs)

	    return grille
	```
  + Expliquer les lignes `except AssertionError:`, `except IndexError:`, `except ValueError:`

  + Que faut-il faire pour qu'un deuxième joueur (ici __machine__) puisse jouer aussi à notre jeu?

  + Faites-le ^^


9. Reste à voir maintenant s'il est encore possible pour le joueur suivant de jouer!

  + Quels sont les cas possibles pour que le joueur suivant puisse jouer?
  + Complétez la fonction `test_poursuite()` qui prendra en paramètre la grille courante:
	``` python
	def test_poursuite(grille):
	    '''
	    Vérifie si la partie peut continuer ou si elle est finie
	    @ param grille: (list) grille à tester pour pouvoir poursuivre
	    return: True ou False selon qu'il soit ou non possible de continuer la partie
	    '''
	    for __?????__:
		if __?????__:
		    return False

	    for __?????__:
		if __?????__:
		    return False

	    if __?????__:
		return False

	    else:
		return True

	```

10. Même principe pour savoir si la partie est finie, complétez la fonction `game_over()`:
	```python
	def game_over(grille,joueur,nom_des_joueurs):
	    '''
	    Détermine la fin d'une partie avec affichage du résultat
	    @ param grille: (list) grille de la partie en cours à l'instant t
	    @ param joueur: (str) type de joueur (humain ou machine)
	    @ param nom_des_joueurs: (str) nom du joueur
	    '''
	    if joueur == 'humain':
		joueur = __?????__
	    else :
		joueur = __?????__
	    if __?????__ == __?????__:
		if __?????__:
		    print('Le joueur {} a gagné'.format(__?????__))
		else:
		    print('__?????__')

	    else:
		print('Les programmeurs ont merdé quelque part... ^^')

	```


11. Il ne vous reste plus qu'à tester votre jeu!




















