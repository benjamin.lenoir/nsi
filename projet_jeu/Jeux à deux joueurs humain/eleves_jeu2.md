**Implémentation du Tic Tac Toe** 
-----


## Objectif de la séance:

- Implémentation d'un jeu (ici Tic Tac Toe)

## Notions abordées et pré-requis:

- **Représentation des données**
	+ Lire et modifier les éléments d’un tableau grâce à leurs index.
	+ Utiliser des tableaux de tableaux pour représenter des matrices : notation a[i][j].
	+ Itérer sur les éléments d’un tableau.

- **Langage et programmation**
	+ Mettre en évidence un corpus conditionnelles, boucles bornées, de constructions élémentaires.
	+ Utiliser la documentation d’une bibliothèque.


## Organisation de la séance

L'activité est guidée au début puis de moins en moins étant donné que ce jeu intervient après le jeu de Nim dans la progression.

## Préparation de l'activité en amont 

Cette activité fait suite à une ou plusieurs séances ayant eu pour but de construire le jeu de Nim avec les élèves, en faisant apparaître le schéma de ce type de jeu à deux joueurs.


1. En reprenant la structure des jeux à deux joueurs, nous allons créer deux fichiers:
	+ une __interface__ qui servira à tout lancement de jeu à deux joueurs tels que nous les avons déjà vus;
	+ un __template__ qui servira de base à toute nouvelle implémentation d'un jeu.

+ Créer un fichier `template.py` qui devra contenir les fonctions suivantes:
	- `situation_initiale()`
	- `situation_courante()`
	- `game_over()`
	- `choix_humain()`
	- `choix_machine()`
	- `test_poursuite()`
dont vous renseignerez la documentation.

**Exemple**
```python
def situation_initiale():
    '''
    Partie à compléter par vos soins
    '''
    pass
```