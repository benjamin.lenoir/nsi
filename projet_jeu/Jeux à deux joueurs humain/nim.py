

def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    '''
    nb_cailloux = int(input('Avec combien de cailloux, voulez-vous jouer ? '))
    print('')
    print('Il y a {} cailloux'.format(nb_cailloux))
    c =''
    for _ in range(nb_cailloux):
        c = c+'O '
    print(c)
    return nb_cailloux


def situation_courante(nb_cailloux):
    '''
    Donne la situation du jeu/plateau à un instant t
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    print('Il reste {} cailloux'.format(nb_cailloux))
    c =''
    for _ in range(nb_cailloux):
        c = c+'O '
    print(c)
    


def game_over(nb_cailloux,joueur,nom_des_joueurs):
    '''
    Détermine la fin d'une partie avec affichage du résultat
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ param joueur: (str) type de joueur (humain ou machine)
    '''
    if joueur == 'humain':
        joueur = nom_des_joueurs[0]
    else :
        joueur = nom_des_joueurs[1]
    if nb_cailloux == 0:
        print('Le joueur {} a perdu'.format(joueur))
    elif nb_cailloux == 1:
        print('Le joueur {} a gagné'.format(joueur))
    else:
        print('Les programmeurs ont merdé quelque part... ^^')



    
def choix_humain(nb_cailloux,nom_des_joueurs):
    '''
    Action du joueur humain selon les possibilités
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    try:
        print('{} , combien prenez-vous de cailloux ?'.format(nom_des_joueurs[0]))
        res = int(input())
        print('')
        assert 1 < res < 4
        
    except AssertionError:
        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3. ')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    return nb_cailloux-res



def choix_machine(nb_cailloux,nom_des_joueurs):
    '''
    Action du joueur machine executé manuellement
    @ param nb_cailloux: (int) nombre de cailloux restants
    '''
    try:
        print('{} , combien prenez-vous de cailloux ?'.format(nom_des_joueurs[1]))
        res = int(input())
        print('')
        assert 1 < res < 4
        
    except AssertionError:
        print('Le nombre de cailloux choisi n\'est pas correct, il faut en choisir 2 ou 3. ')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(nb_cailloux,nom_des_joueurs)
    
    return nb_cailloux-res


def test_poursuite(nb_cailloux):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param nb_cailloux: (int) nombre de cailloux restants (0 ou 1)
    @ return: True si la partie peut continuer, False sinon
    '''
    
    if nb_cailloux >= 2:
        return True
    else:
        return False
