#import nim as jeu
#import tictactoe as jeu


def changement_joueur(joueur):
    '''
    Permet l'alternance du joueur
    @ param joueur: (str) type de joueur (humain ou machine)
    @ return: change le joueur et renvoie humain si erreur d'argument
    '''
    if joueur == 'humain':
        joueur = 'machine'
    else:
        joueur = 'humain'
    return joueur
        
        
        
def jouons():
    '''
    Lance le jeu importé
    '''
    print('Que la partie commence !\n \n')
    joueur = 'humain'
    nom_des_joueurs = [input('Quel est votre pseudo ? '),input('Quel est le pseudo du 2ème joueur ? ')]
    nombre_piece = jeu.situation_initiale()
    nombre_piece = jeu.choix_humain(nombre_piece,nom_des_joueurs)
    jeu.situation_courante(nombre_piece)
    
    while jeu.test_poursuite(nombre_piece):
        joueur = changement_joueur(joueur)
        
        if joueur == 'humain':
            nombre_piece = jeu.choix_humain(nombre_piece,nom_des_joueurs)
        else:
            nombre_piece = jeu.choix_machine(nombre_piece,nom_des_joueurs)
        jeu.situation_courante(nombre_piece)
    
    jeu.game_over(nombre_piece,joueur,nom_des_joueurs)
    

if __name__ == '__main__':

    print()
    print("Jeux proposés :")
    print(" 1 --> Jeu de Nim")
    print(" 2 --> Tic-Tac-Toe")
    print()
    choix = input("Choix du jeu: ")        
    if choix == '1':
        import nim as jeu
        jouons()
    elif choix == '2':
        import tictactoe as jeu
        jouons()
            
    jouons()