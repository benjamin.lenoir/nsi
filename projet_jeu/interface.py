import nim as jeu



def changement_joueur(joueur):
    '''
    Permet l'alternance du joueur
    @ param joueur: (str) type de joueur (humain ou machine)
    @ return: change le joueur et renvoie humain si erreur d'argument
    '''
    if joueur == 'humain':
        joueur = 'machine'
    else:
        joueur = 'humain'
    return joueur
        
        
        
def jouons():
    nombre_piece = jeu.situation_initiale()
    print('Etes-vous prêt à perdre ?')
#    joueur = input()
    joueur = 'humain'
    
#    if joueur == 'humain':
    nombre_piece = jeu.choix_humain(nombre_piece)
    jeu.situation_courante(nombre_piece)
    
    while jeu.test_poursuite(nombre_piece):
        joueur = changement_joueur(joueur)
        
        if joueur == 'humain':
            nombre_piece = jeu.choix_humain(nombre_piece)
        else:
            nombre_piece = jeu.choix_machine(nombre_piece)
        jeu.situation_courante(nombre_piece)
    
    jeu.game_over(nombre_piece,joueur)
    

if __name__ == '__main__':
    jouons()
