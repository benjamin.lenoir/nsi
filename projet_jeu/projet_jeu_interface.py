import projet_jeu_nim as jeu

DIFFICULTE = 3 #on pourra la demander par la suite pour augmenter le niveau de difficulté

def changement_joueur(joueur):
    '''
    Permet l'alternance du joueur
    @ param joueur: (str) type de joueur (humain ou machine)
    @ return: change le joueur et renvoie humain si erreur d'argument
    '''
    if joueur == 'humain':
        joueur = 'machine'
    else:
        joueur = 'humain'
    return joueur

def coeff(joueur):
    '''
    @return : renvoie 1 si il s'agit du joueur actif et -1 sinon
    '''
    if joueur == 'machine' :
        return 1
    else :
        return -1

def min_max(situation, profondeur, joueur):
    '''
    algorithme classique min_max
    @ param : situation est la situation à évaluer par l algo min_max
    @ param : profondeur est le nombre de coups à l avance regardés
    @ param : joueur est le joueur actif dont on évalue la situation
    @ return : renvoie le score optimal obtenu pour le joueur avec la situation donnée et la profondeur choisie ainsi que la configuration
                permettant de l'obtenir
    '''
    if jeu.test_finalite(situation) or profondeur == 0 :
        return jeu.evaluation(situation)*coeff(joueur),situation
    else :
        situations_suivantes = jeu.situ_suiv(situation)
        if joueur == 'machine' :
            L = [ min_max(situ, profondeur-1, 'humain')[0] for situ in situations_suivantes ]
            score = max(L)
        else :
            L = [ min_max(situ, profondeur-1, 'machine')[0] for situ in situations_suivantes ] 
            score = min(L)
        situ_optimale = situations_suivantes[L.index(score)]  
        return score,situ_optimale
        
        
def jouons():
    situation = jeu.situation_initiale()
    print('Etes-vous prêt à perdre ?')
#    joueur = input()
    joueur = 'humain'
    
#    if joueur == 'humain':
    situation = jeu.choix_humain(situation)
    jeu.situation_courante(situation)
    
    while jeu.test_poursuite(situation):
        joueur = changement_joueur(joueur)
        
        if joueur == 'humain':
            situation = jeu.choix_humain(situation)
        else:
            print('\n')
            print('le joueur machine vient de jouer')
            situation = min_max(situation,DIFFICULTE,'machine')[1]
        jeu.situation_courante(situation)
    
    jeu.game_over(situation,joueur)
    

if __name__ == '__main__':
   jouons()

