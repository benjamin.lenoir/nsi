### Création et affichage du plateau de départ ou en cours de jeu

def creer_grille(horizontal,vertical):
    '''
    @ param horizontal: (int) largeur de la grille voulue
    @ param vertical: (int) hauteur de la grille voulue
    return: grille créée selon les dimensions
    '''
    return  [[0]*horizontal]*vertical


def hauteur_grille(grille):
    return len(grille)


def largeur_grille(grille):
    for i in range(len(grille)):
        return len(grille[i])
    
    
def affichage(grille):
    '''
    Affiche sous forme de grille la liste entrée en paramètre
    @ param grille: (list) une grille
    '''
    temp=''
    m=''
    for i in range(largeur_grille(grille)):
        for j in range(hauteur_grille(grille)):        
            if grille[i][j]==0:
                temp='|_|'
            elif grille[i][j]=='X':
                temp='|X|'
            elif grille[i][j]=='O':
                temp='|O|'
            m=m+temp
        print(m,end='\n')
        m=''        


def nb_elements(grille):
    '''
    Retourne le nombre de cases occupées et de cases vides
    @ param grille: (list) une grille
    return: (int) nbre de cases occupées et vides
    '''

    nb_occupees = 0
    nb_vides = 0
    for j in range(largeur_grille(grille)):
        for i in range(hauteur_grille(grille)):
            if grille[i][j] == 0:
                nb_vides+=1
    nb_occupees = largeur_grille(grille)*hauteur_grille(grille) - nb_vides
    
    return nb_occupees, nb_vides
    

def cases_restantes(grille):
    '''
    Retourne le nombre de cases occupées et de cases vides
    @ param grille: (list) une grille
    return: (int) nbre de cases occupées et vides
    '''

    cases_restantes = []
    
    for j in range(largeur_grille(grille)):
        for i in range(hauteur_grille(grille)):
            if grille[i][j] == 0:
                cases_restantes.append((i,j))                
    
    return cases_restantes

### Tic-tac-toe

def situation_initiale():
    '''
    Mise en place du jeu selon les règles
    return: (list) la grille de départ créée selon les dimensions voulues
    '''
    grille=creer_grille(3,3)
    affichage(grille)
    
    return grille

    

def situation_courante(grille):
    '''
    Donne la situation du jeu/plateau à un instant t
    @ param grille: (list) grille du tic-tac-toe actuelle
    '''
    affichage(grille)
    
    


def game_over(grille,joueur):
    '''
    Détermine la fin d'une partie avec affichage du résultat
    @ param grille: (list) grille de la partie en cours à l'instant t
    @ param joueur: (str) type de joueur (humain ou machine)
    '''
    
    if test_poursuite(grille) == False:
        if nb_elements(grille) != 9:
            print('Le joueur {} a gagné'.format(joueur))
        else:
            print('Match nul!')

    else:
        print('Les programmeurs ont merdé quelque part... ^^')




def choix_humain(grille):
    '''
    Action du joueur humain selon les possibilités
    @ param grille: (list) grille du tic-tac-toe avant choix du joueur
    return: (list) la grille modifiée selon le choix du joueur humain
    '''
    try:
        print('Où voulez-vous jouer?')
        abscisse = int(input())
        ordonnee = int(input())
        print('')
        assert 0 <= abscisse <= 2 and 0 <= ordonnee <= 2 and grille[ abscisse ][ ordonnee ] == 0
        grille[ abscisse ][ ordonnee ] = 'X'
        
    except AssertionError:
        print('Cette case n\'existe pas ou est déja occupée. ')
        return choix_humain(grille)
    
    except IndexError:
        print('Cette case n\'existe pas. ')
        return choix_humain(grille)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_humain(grille)
    
    return grille


def choix_machine(grille):
    '''
    Action du joueur machine selon l'algorithme voulu (ici min_max)
    '''
    'En attente du minmax'
    pass



def choix_machine(grille):
    '''
    Action du joueur machine selon l'algorithme voulu (ici min_max)
    '''
    try:
        print('Où voulez-vous jouer?')
        abscisse = int(input())
        ordonnee = int(input())
        print('')
        grille[ abscisse ][ ordonnee ] = 'O'
        assert 0 <= abscisse <= 2 and 0 <= ordonnee <= 2
        
    except AssertionError:
        print('Cette case n\'existe pas ou est déja occupée. ')
        return choix_machine(grille)
    
    except ValueError:
        print('Veuillez saisir un nombre')
        return choix_machine(grille)
    
    return grille



def test_poursuite(grille):
    '''
    Vérifie si la partie peut continuer ou si elle est finie
    @ param grille: (list) grille à tester pour pouvoir poursuivre
    return: True ou False selon qu'il soit ou non possible de continuer la partie
    '''
    for i in range(largeur_grille(grille)):
        if grille[i][0] == grille[i][1] == grille[i][2] == 'X' or grille[i][0] == grille[i][1] == grille[i][2] == 'O':
            return False
        
    for j in range(hauteur_grille(grille)):
        if grille[0][j] == grille[1][j] == grille[2][j] == 'X' or grille[0][j] == grille[1][j] == grille[2][j] == 'O':
            return False
        
    if (grille[0][0] == grille[1][1] == grille[2][2] == 'X' or
        grille[0][2] == grille[1][1] == grille[2][0] == 'X' or
        grille[0][0] == grille[1][1] == grille[2][2] == 'O' or
        grille[0][2] == grille[1][1] == grille[2][0] == 'O'):
        return False
    
    else:
        return True
 
    
        
        

