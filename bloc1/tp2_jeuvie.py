from random import random,randint
from time import sleep

def creer_grille(horizontal,vertical):
    '''
    param horizontal: int
    param vertical: int
    '''
    grille=[0]*vertical
    for i in range(vertical):
        grille[i]=[0]*horizontal
    return grille


def hauteur_grille(grille):
    return len(grille)


def largeur_grille(grille):
    for i in range(len(grille)):
        return len(grille[i])

def creer_grille_aleatoire(horizontal,vertical,p):
    grille=creer_grille(horizontal,vertical)
    for i in range(vertical):
        for j in range(horizontal):
            t=random()
            if t<p:
                grille[i][j]=1
            else:
                grille[i][j]=0
    return grille



grille=[[0,1,0],[1,0,0],[1,1,1]]


def voisins_case(grille,abscisse,ordonnee):
    
    voisins=[]

    M=max(0,abscisse-1)
    m=min(largeur_grille(grille)-1,abscisse+1)
    N=max(0,ordonnee-1)
    n=min(hauteur_grille(grille)-1,ordonnee+1)

    for j in range(N,n+1):
        for i in range(M,m+1):
            if (i,j)!=(abscisse,ordonnee):
                voisins.append(grille[j][i])
    return voisins


def nb_cellules_voisins(grille,abscisse,ordonnee):
    '''
    :param grille: (list) grille sous forme d'une liste de listes
    :param abscisse: (int) abscisse du point autour duquel on cherche les cellules
    :param ordonnee: (int) ordonnee du point autour duquel on cherche les cellules
    :return: nbre de cellules dans les cases voisines
    :CU: aucune
    :Exemples:
    
    >>> nb_cellules_voisins(grille,1,1)
    5
    >>> nb_cellules_voisins(grille,2,2)
    1
    '''
    
    cpteur=0
    voisins=voisins_case(grille,abscisse,ordonnee)
    for j in range(len(voisins)):
        if voisins[j]==1:
            cpteur+=1
    return cpteur


#def afficher_grille(grille):
#    '''
#    :param grille: (list) grille sous forme d'une liste de listes
#    :return: grille sous forme de _ et O
#    :CU: aucune
#    :Exemples:
#    
#    >>> afficher_grille(grille)
#    _ O _
#    O _ _
#    O O O
#    
#    >>> afficher_grille(creer_grille(3,2))
#    _ _ _
#    _ _ _
#
#    '''
#    temp=''
#    m=''
#    for i in range(largeur_grille(grille)+1):
#        print(i)
#        for j in range(hauteur_grille(grille)):        
#            if grille[i][j]==0:
#                temp='_ '
#            else:
#                temp='O '
#            m=m+temp
#        print(m,end='\n')
#        m=''


def generation_suivante(grille):
    '''
    :param grille: (list) grille sous forme d'une liste de listes
    :return: nouvelle grille représentant la génération suivante selon les critères de l'énoncé
    :CU: aucune
    :Exemples:
    
    >>> generation_suivante(grille)
    [[0, 0, 0], [1, 0, 1], [1, 1, 0]]
    >>> generation_suivante([[0, 0, 0], [1, 0, 1], [1, 1, 0]])
    [[0, 0, 0], [1, 0, 0], [1, 1, 0]]
    >>> generation_suivante([[0, 0, 0], [1, 0, 0], [1, 1, 0]])
    [[0, 0, 0], [1, 1, 0], [1, 1, 0]]

    '''   
    grille2=creer_grille(largeur_grille(grille),hauteur_grille(grille))
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):
            if nb_cellules_voisins(grille,i,j)==3:
                grille2[j][i]=1
            elif (nb_cellules_voisins(grille,i,j)<2) or (nb_cellules_voisins(grille,i,j))>3:
                grille2[j][i]=0
            else:
                grille2[j][i]=grille[j][i]
    return grille2
    
    
def evolution_n_generations(grille,n):
    '''
    :param grille: (list) grille sous forme d'une liste de listes
    :param n: (int) entier correspondant au nombre de générations
    :return: nouvelle grille représentant les générations suivantes selon les critères de l'énoncé
    :CU: aucune
    :Exemples:
    
    >>> evolution_n_generations([[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]],4)
    [[0, 0, 0, 0], [0, 1, 1, 1], [1, 1, 1, 0], [0, 0, 0, 0]]
    [[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]
    [[0, 0, 0, 0], [0, 1, 1, 1], [1, 1, 1, 0], [0, 0, 0, 0]]
    [[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]

    '''   
    
    for i in range (1,n+1):
        grille2=generation_suivante(grille)
        grille=grille2
        print(grille2)
        sleep(2)
    
    
    
    
import doctest
doctest.testmod(verbose=False)
