#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`Time` module

:date: Juin, 2019

Module for `Time` representation.

"""
from collections import namedtuple

Time=namedtuple('Time',('hours','minutes','seconds'))

def create(hours,minutes,seconds):
    """
    :param hours: number of hours in time of a competitor
    :type name: int
    :param minutes: number of minutes in time of a competitor
    :type name: int
    :param seconds: number of seconds in time of a competitor
    :type name: int
    :return: time of a competitor
    :rtype: Time
    :UC:
    """
    
#    assert hours in range(24) and minutes in range (60) and seconds in range(60)
    return Time(hours,minutes,seconds)

def get_hours(time):
    """
    :param time: time of a competitor
    :type name: Time
    :return: number of hours in time of a competitor
    :rtype: int
    """
    return time[0]
    

def get_minutes(time):
    """
    :param time: time of a competitor
    :type name: Time
    :return: number of minutes in time of a competitor
    :rtype: int
    """
    return time[1]


def get_seconds(time):
    """
    :param time: time of a competitor
    :type name: Time
    :return: number of seconds in time of a competitor
    :rtype: int
    """
    return time[2]


def compare(time1,time2):
    """
    :param time1: time of a competitor
    :type name: Time
    :param time2: time of a competitor
    :type name: Time
    :return: 1 or -1 or 0
    :rtype: int
    """
    if time1<time2:
        return 1
    elif time1>time2:
        return -1
    elif time1==time2:
        return 0
    

def to_string(time):
    """
    :param time: a time
    :type time: Time
    :return: a string representation for given competitor
    :rtype: str
    """
    return "{} heures, {} minutes et {} secondes".format(time[0],time[1],time[2])

    
if __name__ == '__main__':
    pass 