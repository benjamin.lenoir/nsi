import Competitor
import Time

def read_competitors(fichier):
    '''
    :param fichier: (csv) fichier contenant les données à trier
    return: (dict) l'ensemble des participants avec leur numéro de dossard
    '''
    participants={}
    
    try:
        with open('{}.csv'.format(fichier),'r')as entree:
            
            cle = entree.readline()
            lignes_lues = entree.readlines()
            nb_dossard=1

        for ligne in lignes_lues:
            comp = ligne.rstrip('\n').split(';')
            candidat=Competitor.create(comp[0],comp[1],comp[2],comp[3],nb_dossard)        
            participants.update({nb_dossard:candidat})
            nb_dossard+=1
     
        return participants
    
    except FileNotFoundError:
        return 'Fichier inexistant'


def aff_comp(competitor):
    
    liste=list(competitor.values())
    
    for i in range (len(liste)):
        print(Competitor.to_string(liste[i]))
    
    
def select_competitor_by_bib(ensemble_participants,nb_dossard):
    '''
    :param ensemble_participants: (dict) ensemble des participants
    :param nb_dossard: (int) numéro du dossard voulu
    return: (competitor) le participant correspondant au numéro de dossard
    '''
    try:
        return ensemble_participants[nb_dossard]
    
    except KeyError:
        return 'Ce dossard n\'a pas été attribué'
    

def select_competitor_by_year(ensemble_participants,annee):
    '''
    :param ensemble_participants: (dict) ensemble des participants
    :param annee: (str) année de naissance voulue
    return: (competitor) le participant correspondant à l'année voulue
    '''
    for i in range(1,len(ensemble_participants)+1):
        date=Competitor.get_birthdate(ensemble_participants[i])
            
        if date.endswith(annee):
            print(ensemble_participants[i])
                
    
def select_competitor_by_name(ensemble_participants,nom):
    '''
    :param ensemble_participants: (dict) ensemble des participants
    :param nom: (str) nom recherché
    return: (competitor) le participant correspondant au nom recherché
    '''
    for i in range(1,len(ensemble_participants)+1):
        name=Competitor.get_lastname(ensemble_participants[i])
            
        if name.endswith(nom):
            print(ensemble_participants[i])
                
  
  
  
  


def read_performances(fichier):
    '''
    :param fichier: (csv) fichier contenant les données à trier
    return: (dict) l'ensemble des participants avec leur numéro de dossard
    '''
    temps={}
    
    try:
        with open('{}.csv'.format(fichier),'r')as entree:
            
            cle = entree.readline()
            lignes_lues = entree.readlines()
            nb_dossard=1

        for ligne in lignes_lues:
            comp = ligne.rstrip('\n').split(';')
            nb_dossard=int(comp[0])
            candidat=Time.create(comp[1],comp[2],comp[3])        
            temps.update({nb_dossard:candidat})
     
        return temps
    
    except FileNotFoundError:
        return 'Fichier inexistant'


def aff_temps(perf):
    for bib_num,val in perf.items():
        print(bib_num+' : '+Time.to_string(perf[bib_num]))


        
def set_performances(competitors,performances):

    for cle in performances.keys():
        competitors[cle]['performance']=Time.to_string(performances[cle])
    print(competitors)

  

def print_results(competitor):
    
    liste=list(competitor.values())
    
    for i in range (len(liste)):
        if Competitor.to_string(liste['performance'])!=None:
            print(Competitor.to_string(liste[i]) +' => '+Time.to_string(liste['performance']))        
        else:
            print(Competitor.to_string(liste[i]) +' => ')
        
  
  
test=read_competitors('small_inscrits')    
test2=read_performances('small_performances')
#affichage(test)
    