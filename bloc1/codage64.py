BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']


def to_base64(triplet):
    binaire=[]    
    for nbre in triplet:

        for i in range(7,-1,-1):
            if nbre >= 2**i:
                reste = nbre%2
                nbre = nbre//2
                binaire.append(reste)
            else:
                binaire.append(0)
            
    return str(binaire)
        
    
      
    
#import doctest
#doctest.testmod(verbose=False)    