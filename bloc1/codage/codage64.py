BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']




def to_bin(octet):
    '''
    Transforme un octet en binaire sous forme de chaine
    '''
    chaine=''
    for i in range(7,-1,-1):
        
        if octet>=2**i:
            reste=octet%2
            octet=octet//2
            chaine+=str(reste)

    if len(chaine)<8:
        chaine=(8-len(chaine))*str(0)+chaine
                        
    return chaine




def to_paquet(chaine):
    '''
    Transforme une chaine en une liste de paquets de 6
    '''
    paquet=[]
    
    for i in range(0,len(chaine),6):
        
        paquet.append(chaine[i:i+6])
       
    if len(chaine)%6==2:
        paquet[-1]+='0000'
            
    elif len(chaine)%6==4:
        paquet[-1]+='00'
            
    return paquet
    


def from_bin(binaire):
    '''
    Transforme un binaire sous forme de chaine en decimal
    '''
    l=len(binaire)
    res=0
    
    for i in range(l):
        res+=int(binaire[i])*2**(l-i-1)
        
    return res
    
    
def from_bin_liste(liste):
    
    res=[]
    for nbre in liste:
        res.append(from_bin(nbre))
    return res
    



def to_base64(triplet):
    
    chaine=''
    liste=[]
    
    for nbre in triplet:
        binaire=to_bin(nbre)
        chaine+=binaire
    
    liste=to_paquet(chaine)
    from_bin_liste(liste)
    
    for i in range(64):
        if 
    return chaine





