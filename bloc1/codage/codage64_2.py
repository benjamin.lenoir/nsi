BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']





#def to_base64(triplet):
#    '''
#    convertit le triplet d'octets en une chaîne de quatre symboles
#    
#    :param triplet: (tuple ou list) une séquence d'octets
#    :return: (str) la chaîne de symboles de la base 64 représentant le triplet d'octets
#    :CU: 1 <= len(triplet) <= 3 et les entiers de triplet tous compris entre 0 et 255
#    :Exemple:
#    
#    >>> to_base64((18, 184, 156))
#    'Eric'
#    >>> to_base64((18, 184))
#    'Erg='
#    >>> to_base64((18,))
#    'Eg=='
#    '''
    
    

def to_bin(octet):
    '''
    transforme un octet en binaire sous forme de chaine
    :param octet: (int) octet
    :return: (str) binaire sous forme de chaine de longueur 8 (octet)
    
    '''
    chaine=''
    for i in range(7,-1,-1):
        
        if octet>=2**i:
            reste=octet%2
            octet=octet//2
            chaine=str(reste)+chaine
    
    
    if len(chaine)<8:
        chaine=(8-len(chaine))*str(0)+chaine
                        
    return chaine


def to_paquet(chaine):
    '''
    Transforme une chaine en une liste de paquets de 6
    '''
    paquet=[]
    
    for i in range(0,len(chaine),6):
        
        paquet.append(chaine[i:i+6])
       
    if len(chaine)%6==2:
        paquet[-1]+='0000'
            
    elif len(chaine)%6==4:
        paquet[-1]+='00'
            
    return paquet


