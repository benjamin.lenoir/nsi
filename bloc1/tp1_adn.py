from random import choice


def estADN(val):        # Vérifie si la séquence contient les quatres bases A, C, G et T         
    adn='ACGT'
    for test in val:
        if test not in adn:
            return False
    return True


def genereADN(taille):
    '''
    Génère aléatoirement une séquence d'ADN
    '''
    
    
    adn=''
    for i in range(taille):
        s=choice(['A','G','T','C'])
        adn=adn+s
    return adn


def baseComplementaire(base,support):   #Renvoie la base complementaire selon qu'ait ADN ou ARN au départ
    if base is 'C':
        return 'G'
    elif base is 'G':
        return 'C'
    elif base is 'T' and support=='ADN':
        return 'A'
    elif base is 'U' and support=='ARN':
        return 'A'
    elif base is 'A':
        if support=='ADN':
            return 'U'
        else:
            return 'T'
    return False


def transcrit(sequence,debut,fin):
    '''
    Renvoie l'ARN correspondant à une séquence d'ADN
    '''
    seq=sequence[debut:fin]
    newseq=''
    for i in range(len(seq)):
        newseq=newseq+baseComplementaire(seq[i],'ADN')
    return newseq


def codeGenetique(codon):
    '''
    Acide aminé correspondant abrégé en une lettre
    '''
    if codon in ['UUU', 'UUC']:
        return 'F'
    elif codon in ['UUA', 'UUG', 'CUU', 'CUC', 'CUA', 'CUG']:
        return 'L'
    elif codon in ['AUU', 'AUC', 'AUA']:
        return 'I'
    elif codon in ['AUG']:
        return 'M'
    elif codon in ['GUU', 'GUC', 'GUA', 'GUG']:
        return 'V'
    elif codon in ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC']:
        return 'S'
    elif codon in ['CCU', 'CCC', 'CCA', 'CCG']:
        return 'P'
    elif codon in ['ACU', 'ACC', 'ACA', 'ACG']:
        return 'T'
    elif codon in ['GCU', 'GCC', 'GCA', 'GCG']:
        return 'A'
    elif codon in ['UAU', 'UAC']:
        return 'Y'
    elif codon in ['UAA', 'UAG', 'UGA']:
        return '*'
    elif codon in ['CAU', 'CAC']:
        return 'H'
    elif codon in ['CAA', 'CAG']:
        return 'Q'
    elif codon in ['AAU', 'AAC']:
        return 'N'
    elif codon in ['AAA', 'AAG']:
        return 'K'
    elif codon in ['GAU', 'GAC']:
        return 'D'
    elif codon in ['GAA', 'GAG']:
        return 'E'
    elif codon in ['UGU', 'UGC']:
        return 'C'
    elif codon in ['UGG']:
        return 'W'
    elif codon in ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG']:
        return 'R'
    elif codon in ['GGU', 'GGC', 'GGA', 'GGG']:
        return 'G'


def traduit(seq):
    '''
    Renvoie la séquence protéique obtenue par la traduction de la séquence ARN 
    '''
    newseq=''
    for i in range(0,len(seq),3):
        newseq=newseq+codeGenetique(seq[i:i+3])
    return newseq


def replique(seq):
    newseq=''
    newseq2=''
    test=''
    test2=''
    test3=''
    for k in range(len(seq)):
        newseq=newseq+baseComplementaire(seq[k],'ADN')
    for j in range(0,len(newseq),2):
        test=newseq[j:j+2]
        test2=test2+test[::-1]
    for i in range(0,len(test2),1):
        newseq2=newseq2+baseComplementaire(test2[i],'ARN')
    return newseq2
