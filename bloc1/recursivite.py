def add(a,b):
    if b==0:
        return a
    else:
        return add(a,b-1)+1
    

def mult(a,b):
    if b==0:
        return 0
    else:
        return mult(a,b-1)+a


def power(a,b):
    if b==0:
        return 1
    else:
        return power(a,b-1)*a
    

def euclide(a,b):
    if a%b==0:
        return b
    else:
        return euclide(b,a%b)
    
