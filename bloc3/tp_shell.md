**Initiation au Shell et gestion des répertoires** 
-----


### L'objectif de la séance est de comprendre le fonctionnement des répertoires et de se familiariser avec leur manipulation sous Unix.

- **Architectures matérielles et système d'exploitation**
	+ Utiliser les commandes de base en ligne de commande.

- **Pré-requis**
	+ Terminal de commande
	+ Création et manipulation d'un fichier

<!-- - **Lien utile** : [La page wikipedia des commandes Unix][lien wiki].-->


## Qu'est-ce qu'un fichier?

Un fichier est un document conservé sur disque (disque dur, clé USB, CD-ROM,  etc.). 
Ce document peut contenir du texte, des images, des sons, de la vidéo, des programmes, représentant chacun un type de fichier différent. 
Le type d’un fichier est donné par son extension. L’extension est la suite de lettres situées après le point.

- **Quelques exemples :**
	+	.txt, .docx, .odt sont les extensions pour les fichiers texte, Word et Libreoffice
	+	.xlsx est l’extension pour les fichiers Excel
	+	.jpg et .png sont les extensions pour les fichiers ...........
	+	.mp3 et .m4a sont les extensions pour les fichiers ...........
	+	.mp4 et .mov sont les extensions pour les fichiers ...........

## Comment organiser l'ensemble des fichiers?

On pourrait mettre tous les fichiers sur le bureau de son ordinateur mais il serait difficile de retrouver rapidement ce que l’on cherche si le nombre de fichiers est important. On a donc besoin de classer les fichiers de telle sorte qu’on puisse les retrouver facilement.
Un répertoire (appelé aussi dossier) est tout simplement un endroit de rangement de nos fichiers informatiques.
On pourra ainsi créer un répertoire  “Banques” et encore un répertoire “Assurances” pour ranger les fichiers correspondant à ces catégories.
En informatique, un répertoire ou un dossier est une liste de descriptions de fichiers. Du point de vue du système de fichiers, il est traité comme un fichier dont le contenu est la liste des fichiers référencés. Un répertoire a donc les mêmes types de propriétés qu'un fichier comme le nom, la taille, la date, les droits d'accès et les divers autres attributs.


## Ok, mais comment je me repère dans tous les dossiers et fichiers?

Pour comprendre l’organisation d’un disque dur, on peut imaginer une armoire contenant des tiroirs contenant des chemises (ou pochettes) qui, à leur tour, contiennent des sous-chemises, qui, à leur tour, peuvent contenir des sous-chemises et ainsi de suite.
Il faut imaginer que chacun de ces contenants peut également contenir des feuilles.

En informatique, on retrouve la même organisation en arborescence :
	-	le disque dur de l’ordinateur c’est l’armoire,
	-	les répertoires sont les tiroirs et les chemises,
	-	les sous-répertoires  sont les sous-chemises,
	-	les  fichiers informatiques (documents texte, image, vidéo…) sont les feuilles.

Par exemple, physiquement, si je veux retrouver ma facture de Gaz de 2012, 
j’ouvrirais l’armoire,
puis le tiroir document, 
puis la chemise EDF,Gaz,
puis je prendrai la feuille Gaz 2012
D’un point de vue informatique, 
je partirai du disque dur, 
puis repertoire Documents, 
puis sous-repertoire EDF-Gaz 
puis fichier Gaz 2012





















![verifier][verifier] **Faire vérifier votre travail par le professeur.**

## ![dossier][dossier] Seconde partie : Gérer les droits de permission.

**1. Placer vous maintenant dans le répertoire France.**

**2. Utiliser l’éditeur ```nano``` (ou pico) pour éditer le fichier paris :**

![question][question] Écrire : "Paris est la capitale de la France".

![question][question] Enregistrer le fichier ainsi édité.

![question][question] Faire une copie du fichier paris sous le nom capitale.

![question][question] Visualiser le contenu du fichier paris à l’aide de la commande ```cat```. 

![question][question] Utiliser la commande ```ls-l``` pour afficher les caractéristiques des fichiers paris, lille et capitale.

**3. Les droits d’accès**


![droit][droit]


| **La première lettre désigne le type de fichier** | **Les droits des fichiers** |
| :------------------------------------------:  | :---------------------: |
| ```-``` : fichier "classique" | ```r``` : read (droit de lecture) |
| ```d``` : répertoire (directory) | ```w``` : write (droit d'écriture) |
| ```l``` : lien symbolique (link) | ```x``` : execute (droit d'exécuter un fichier ou d'ouvrir un répertoire)|


![question][question] Donner la signification de ```-rw-rw-r--```?


**4. On souhaite modifier ces droits de manière à obtenir : **```-rw-r--r--```.

![question][question] On pourra tester par exemple l’effet de la commande ```chmod og -r paris```.

![question][question] Vérifier la modification des droits en tapant de nouveau ```ls -l```.

![question][question] Modifier de nouveau les droits de manière à obtenir ```-rw-r--r--```.

![question][question] puis de manière à obtenir ```-rw-rw-rw-```.

 
**5. Notation numérique et système octal**

| **Droit**	| **Valeur alphanumérique** | ![question][question] **Valeur octale à compléter** | **Valeur binaire** |
| :------: | :------: | :---------------: | :------: |
| aucun droit |	---	| ...| 000 |
| exécution seulement |	--x | ... | 001 |
| écriture seulement| -w- | ... |	010 |
| écriture et exécution	| -wx |	... |	011 |
| lecture seulement | r-- | ... | 100 |
| lecture et exécution | r-x | ...  | 101 |
| lecture et écriture | rw-	| ...	| 110 |
| tous les droits (lecture, écriture et exécution) | rwx | ...	| 111 | 

![verifier][verifier] **Faire vérifier votre travail par le professeur.**

En combinant 3 chiffres décimaux on peut changer le mode d’accès pour l’ensemble des propriétaires.

![question][question] On pourra tester la commande : ```chmod 777 paris```.

![question][question] puis ```chmod 000 paris```.

![question][question] Vérifier la validité de cette dernière commande en tapant de nouveau ```ls-l``` puis en entrant la commande ```cat```.

![question][question] Utiliser ensuite la commande décimale pour protéger votre fichier en mode **"parano"** avec ```-rw-------```.

![question][question] Puis en mode standard avec ```-rw-r--r--```.

**5. la gestion des droits s’applique aussi aux répertoires**

![question][question] Modifier les droits du répertoire france afin que seul le propriétaire puisse lire, modifier et exécuter le contenu de celui-ci.

**6. Challenge Python**

On donne les fonctions suivantes :

![python][python]

![question][question] Puis en mode standard avec Réaliser la docString et les docTest pour ces deux fonctions.

![question][question] Utiliser ces fonctions pour vérifier certaines de vos réponses données dans cette activité.

![verifier][verifier] **Faire vérifier votre travail par le professeur.**

## QCM : Je me teste avant l'évaluation.

[![qcm][qcm]](http://thierry-sautiere.000webhostapp.com/darkshellqcm/darkQuiz.html)