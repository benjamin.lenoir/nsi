---
title: Fiche élève sur le shell
subtitle : Bloc 3
author:  B. Lenoir et S. Malicet 
date: Juillet 2019
---


**Initiation au Shell et gestion des répertoires** 
-----


## Objectif de la séance:

- Comprendre le fonctionnement des répertoires et de se familiariser avec leur manipulation sous Unix.

## Notions abordées et pré-requis:

- **Architectures matérielles et système d'exploitation**
	+ Utiliser les commandes de base en ligne de commande.

- **Pré-requis**
    + Utilisation connue de VirtualBox sous Ubuntu
	+ Terminal de commande déjà connu et utilisé
	+ Création et manipulation d'un fichier

- **Compétences Transversales** :
    + Justifier ses choix

- **Notions vues lors d'un cours précédent mais non utilisées**
    + Base de système de fichiers
    + Manipulation du système de fichiers (commandes sur création, suppression, déplacement)

 
## Qu'est-ce qu'un fichier?

Un fichier est un document conservé sur disque (disque dur, clé USB, CD-ROM,  etc.) qui peut contenir du texte, des images, des sons, de la vidéo, des programmes, représentant chacun un type de fichier différent. 

### Type d'un fichier

Le type d’un fichier est donné par son extension. L’extension est la suite de lettres situées après le point.

- **Quelques exemples :**

	+	`.txt`, `.docx`, `.odt` sont les extensions pour les fichiers texte, Word et Libreoffice
	+	`.xlsx` est l’extension pour les fichiers Excel

**Questions:**
	
1. Quels sont les extensions des fichiers images les plus courantes?
	
2. Celles des fichiers audio?
	
3. De quels types de de fichiers `.mp4` et `.mov` sont-elles les extensions?

### Comment organiser l'ensemble des fichiers?

On pourrait mettre tous les fichiers en vrac sur son ordinateur mais il serait difficile de retrouver rapidement ce que l’on cherche si le nombre de fichiers est important.   

On a donc besoin de classer les fichiers de telle sorte qu’on puisse les retrouver facilement.

Un répertoire (appelé aussi dossier) est tout simplement un endroit de rangement pour nos fichiers informatiques.

Pour être plus précis, en informatique, un répertoire ou un dossier est une liste de descriptions de fichiers. Du point de vue du système de fichiers, il est traité comme un fichier dont le contenu est la liste des fichiers référencés. 

Un répertoire a donc les mêmes types de propriétés qu'un fichier comme le nom, la taille, la date, les droits d'accès et les divers autres attributs.


### Ok, mais comment je me repère dans tous les dossiers et fichiers?

Pour comprendre l’organisation d’un disque dur, on peut imaginer une armoire contenant des tiroirs contenant des chemises (ou pochettes) qui, à leur tour, contiennent des sous-chemises, qui, à leur tour, peuvent contenir des sous-chemises et ainsi de suite.

Il faut imaginer que chacun de ces contenants peut également contenir des feuilles.

En informatique, on retrouve la même organisation en ***arborescence*** :
- le disque dur de l’ordinateur ----> l’armoire,
- les répertoires ----> les tiroirs et les chemises,
- les sous-répertoires ----> les sous-chemises,
- les fichiers informatiques (documents texte, image, vidéo…) ----> les feuilles.

L’arborescence classique sous Unix ressemble à l'image ci-dessous:

![arborescence](./arbo.png)

#### Remarque: 
- Le répertoire racine est particulier puisqu’il est le seul à n’être contenu dans aucun répertoire.
- On appelle répertoire parent (ou supérieur) le répertoire dans lequel est directement contenu notre élément.


**Questions:**

4. 	- Quel est le répertoire parent du répertoire `projet1`?
   	- Quel est le répertoire parent du fichier `d1`?

5. Que donne la commande `ls` depuis `/home`?

6. Tester les différentes options `ls -s`, `ls -l` et `ls -a` et lister leurs effets.
<!--ls donne le contenu du répertoire où l'on se trouve, dans l'ordre suivant : symboles, chiffres, majuscules, minuscules. Les fichiers apparaissent sous leur nom, les sous-répertoires ont leur nom suivi d'un slash (par exemple, machin/). Les noms de fichiers suivis d'une étoile sont des exécutables.
Il est possible d'obtenir des niveaux de détails plus importants grâce aux options suivantes :
•	ls -s liste les fichiers ainsi que leurs tailles en kilo-octets.
•	ls -l liste l'ensemble des informations inhérentes à chacun des fichiers.
•	ls -a liste tous les fichiers (même ceux commençant par  .  , c’est-à-dire les fichiers cachés).-->

7. Autre possibilité, utiliser la commande `tree`. Quel peut être son avantage?


### Comment peut-on se diriger dans cette arborescence?

On utilise ce qu'on appelle un ***chemin*** (ou ***path***), ie: un enchaînement de répertoires imbriqués, séparés par le caractère `/`, avec un fichier ou un répertoire à la fin (`dossier/dossier` ou `dossier/fichier`).

**Exemples:**
    
+ si je me trouve sur la racine `root` et que je veux accéder au dossier `durand` je dois suivre le chemin: 
	```
    /USERS/durand
    ```
	
+ si je me trouve dans le répertoire `USERS` et que je veux accéder au fichier `p1` je dois suivre le chemin: 
    ```
    /dupond/projet1/p1
    ```

Dans les deux cas, ce sont des chemins dits *relatifs* (sous-entendu au dossier courant).

Parfois, il sera utile d'utiliser des chemins dits *absolus*, ie qui ne prennent pas en compte notre position dans l’arborescence. En reprenant le dernier exemple, le chemin absolu est: 
    ```
    /USERS/dupond/projet1/p1
    ```

Il est aussi possible de remonter vers un répertoire parent ou de descendre dans l'arborescence à tout moment en utilisant respectivement les commandes `cd ..` ou `cd nom_dossier`


###	Commandes de manipulation de répertoires.

**Questions:**
	
8. 	- Placez vous dans votre répertoire `/home`
	- Créez un répertoire nommer `test` à l'aide de la commande `mkdir nom_dossier`
	- Visualisez le contenu de votre dossier puis créez deux dossiers dans `home`, appelés `nsi` et `essai` et visualisez à nouveau le contenu du dossier pour confirmer la présence ou non des dossiers.

9.	Supprimez le répertoire `essai` à l'aide de la commande `rmdir nom_dossier`

10. - Placez-vous dans le dossier `test` et visualisez son contenu.
	- Sortez du dossier `test` et retournez dans le répertoire initial à l'aide de la commande `cd ..` 

11. Quelle est l'utilité de la commande `pwd`?

Il est aussi possible de manipuler les fichiers:

12. - Placez-vous dans le dossier `test`  puis créez un fichier `nouveau` (à l'aide de `touch nom_fichier`).
	- Exécutez  `mv nouveau  .`  Que s’est-il passé ?
	- A nouveau dans le dossier `test` , exécutez  `mv nouveau nsi`. Que s’est-il passé ?
	- A quoi sert la commande `mw` au final?


## Exercices : 

**Exercice 1:**

Où que vous soyez, quel est l'effet de la commande cd sans paramètre ?

**Exercice 2:**

1. Créez un répertoire `nsi` sous votre répertoire de travail, puis un répertoire `tp1` sous `nsi` 
2. Effacez le répertoire `nsi` avec la commande `rmdir`. Que constatez-vous ? 
3. Après avoir effacé les répertoires `tp1` et `nsi`, créez à l'aide d'une seule commande les répertoires `nsi`, `nsi/tp1`, `nsi/tp2` 
4. Renommez le répertoire `nsi` en `test` 
5. Effacez à l'aide d'une seule commande les répertoires `test/tp1` et `test/tp2`

**Exercice 3:**

1. Combien y a-t-il de noms de répertoires dans la racine ? 
2. Donnez un exemple de nom de fichier se trouvant dans votre répertoire personnel :  	
   - par un chemin relatif    
   - par un chemin absolu.


**Exercice 4:**

![arborescence2](./arbo2.png)

Dans votre répertoire d'accueil (`/home/user` par exemple), créez l'arborescence précédente, en n'utilisant que des chemins relatifs puis vérifiez (à l'aide de `tree`)

=========

* 1 - Quelle commande utiliser pour connaitre l'endroit où se situer dans les répertoires :
* [ ] pwd
* [ ] ls -al
* [ ] ctrl c

* 2 - Pour afficher vos fichiers cachés, vous utilisez la commande (plusieurs choix possibles):
* [ ] ls -a
* [ ] ls -d
* [ ] ls -al
* [ ] ls -C

* 3 - Pour aller du répertoire _/home/user/nsi/group_ au répertoire _/home/user/maths_, quelle commande doit-on saisir? 
* [ ] _cd .._
* [ ] _cd maths_
* [ ] _cd ../maths_
* [ ] _cd_

* 4 - Pour affichez le contenu du dossier `/home/nsi/group`, vous utilisez la commande :
* [ ] ls -a
* [ ] mkdir group
* [ ] cd group
* [ ] mw group home

* 5 - Pour affichez tous les types de fichier du repertoire courant, vous utilisez la commande :
* [ ] type *    
* [ ] mkdir *
* [ ] cat *

* 6 - Affichez la liste des fichiers du répertoire `/nsi`, vous utilisez la commande :
* [ ] ls /nsi
* [ ] ls nsi
* [ ] ls -nsi