# Simulation d'un système proie-prédateur

## Présentation possible de Wa-tor

........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................


### Préparation de la mer

La mer sera représentée par un tableau à deux dimensions où l'élément mer[i][j] représentera la case située à la ligne i et à la colonne j.

On suppose les fonctions `creer_grille`, `hauteur_grille` et `largeur_grille`  déjà vues ou créées et on suppose que les cases vides, les thons et les requins sont respectivement représentés par des 0, des 1 et des 2.

1) Écrire une fonction `creer_grille_aleatoire` qui prend en paramètre une grille et qui renvoit la même grille remplie aléatoirement de 0 et 1.

2) Nous voulons maintenant représenter la situation à l'aide d'une grille contenant aléatoirement une certaine quantité de thons (30%) et de requins (10%)
Pour cela, modifier la fonction précédente pour qu'elle prenne en paramètre la proportion de poissons souhaitée selon le type et renvoie la grille modifiée en conséquence.

3) Comme la mer est supposée torique (pour simplifier la situation), chaque case/cellule est constituée de quatre cases/cellules voisines vers lesquelles chaque poisson pourra potentiellement se déplacer.

Ecrire une fonction `voisins_cellule` qui renvoie une liste constituée des coordonnées de chaque cellule voisine de la case de la grille dont les coordonnées sont données en paramètre:

```python
>>> grille = [[2,0,0],[1,2,0],[0,2,0]]
>>> voisins_cellule(grille,1,2)
[(0, 2), (2, 2), (1, 0), (1, 1)]
```

4) Ce qui nous intéressera par la suite, ce sera de savoir vers quelle case précisément notre thon ou requin va pouvoir se déplacer.
Ecrire une fonction `cases_dispos` qui renvoie une liste constituée des coordonnées des voisins de la case dont la valeur donnée en paramètre correspond au poisson cherché.
```python
>>> grille = [[2,0,0],[1,2,0],[0,2,0]]
>>> cases_dispos(grille,1,2,2)
[(1, 1)]
```

### Comportement des poissons

1) Écrire une fonction `gestation` qui crée une grille contenant les valeurs de gestation des différentes espèces selon la grille représentant la mer et les valeurs initiales de gestation données en paramètre.
```python
>>> gestation([[1,0,2],[0,0,1],[2,2,1]],2,6)
 [[6, 0, 2], [0, 0, 6], [2, 2, 6]]
```

2) De même, Écrire une fonction `energie` qui crée une grille contenant les valeurs d'énergie des différentes espèces selon la grille représentant la mer et les valeurs initiales d'énergie données en paramètre.
```python
>>> energ([[1, 0, 2], [0, 0, 1], [2, 2, 1]],3)
[[3, 0, 0], [0, 0, 3], [0, 0, 3]]
```


3) Écrire une procédure `thon_seul` qui va gérer les différents déplacmeents possibles pour un seul thon selon la mer.

3) Écrire une procédure `requin_seul` qui va gérer les différents déplacmeents possibles pour un seul thon selon la mer.

### Evolution des différentes population

1) Écrire une fonction `nb_poissons` qui va compter le nombre de poissons selon leur espèce à chaque simulation

2) Reste à compléter votre programme afin qu'il affiche l'évolution de la population de chaque espèce au bout de n simulations.