from jeuvie import *
from comportement import *
from tkinter import *
import matplotlib.pyplot as plt



def nb_poissons(grille):
    '''
    Compte le nombre de thons et de requins à un instant t dans la mer
    :param grille: (list) liste de liste
    return: (int) deux entiers
    '''
    cpt1=0
    cpt2=0
    
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):
            
            if grille[i][j]==1:
                cpt1+=1
                
            elif grille[i][j]==2:
                cpt2+=1
            
    return cpt1,cpt2


def evolution_n_generations(grille,n):
    '''
    Simule l'évolution de la mer au bout de n simulations
    :param grille: (list) liste de liste
    :param n: (int) nombre de simulations
    '''
    gestation_init_thon=2
    gestation_init_req=6
    energie_init=3
    
    ges=gestation(grille,gestation_init_thon,gestation_init_req)
    energie=energ(grille,energie_init)

    
    thons=[]
    requins=[]

    N=n*largeur_grille(grille)*hauteur_grille(grille)

    for i in range (0,N):
        
        (grille,ges)=thon_seul(grille,ges,gestation_init_thon)
        (grille,ges,energie)=requin_seul(grille,ges,gestation_init_req,energie,energie_init)
        
        nb_req,nb_thon=nb_poissons(grille)
        thons.append(nb_thon)
        requins.append(nb_req)
    
    x=[j for j in range(0,N)]
    y=thons
    z=requins
    
    plt.title('Evolution des populations')
    plt.xlabel('Taille des populations')
    plt.ylabel('Numéro du pas')
    plt.grid()
    plt.plot(x,y)
    plt.plot(x,z)
    plt.show()


grille_test=creer_grille_aleatoire(25,25,0.1,0.3)

fenetre=Tk()

fenetre.title('Evolution de la mer')
terrain=Canvas(fenetre,height=600,width=800)
terrain.pack()
carreau=[[terrain.create_rectangle(i*20,j*20,(i+1)*20,(j+1)*20,fill='#0000FF')
              for i in range(largeur_grille(grille_test))] for j in range(hauteur_grille(grille_test))]

def evol_graphique(grille):

    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):
            terrain.itemconfigure(carreau[i][j],fill='#0000FF')
            if grille[i][j]==1:
                terrain.itemconfigure(carreau[i][j],fill='#AFAFAF')
            elif grille[i][j]==2:
                terrain.itemconfigure(carreau[i][j],fill='#EE1010')

bouton=Button(fenetre,text='pfff',command=evol_graphique(grille_test))
bouton.pack()
              
fenetre.mainloop()