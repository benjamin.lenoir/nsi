from random import random
from jeuvie import *

def creer_grille_aleatoire(horizontal,vertical,prop1,prop2):
    """
    Remplissage aléatoire de la mer avec les proportions voulues de requins et de thons
    
    :param horizontal: (int) largeur de la grille à créer
    :param vertical: (int) hauteur de la grille à créer
    :param propthon: (float) proportion de requin voulue (1)
    :param propreq: (float) proportion de thon voulue (2)
    :return: une grille créée aléatoirement contenant des 0, 1 et 2 selon les proportions voulues
    :effets de bord: la grille représentant la mer est modifiée
    """

    grille=creer_grille(horizontal,vertical)
    c1=0
    c2=0
    for j in range(vertical):
        for i in range(horizontal):
            t=random()
            if t<prop1:
                grille[i][j]=1
                c1+=1
            elif t-prop1<prop2:
                grille[i][j]=2
                c2+=1
            else:
                grille[i][j]=0
    
    return grille



def voisins_cellule(grille,abscisse,ordonnee):
    '''
    Renvoie une liste constituée des coordonnées de chaque voisin de la case dont les coordonnées sont données
    :param grille: (list) une liste de liste 
    :param abscisse: (int) abscisse du point autour duquel on cherche les cellules
    :param ordonnee: (int) ordonnee du point autour duquel on cherche les cellules
    :return: (list) liste de coordonnées
    
    >>> grille = [[2,0,0],[1,2,0],[0,2,0]]
    >>> voisins_cellule(grille,1,2)
    [(0, 2), (2, 2), (1, 0), (1, 1)]
    '''
    
    x,y=abscisse,ordonnee
    l=largeur_grille(grille)
    h=hauteur_grille(grille)

    voisin_haut   = ((x-1)%h,y)
    voisin_gauche = (x,(y-1)%l)
    voisin_droite  = (x,(y+1)%l)
    voisin_bas    = ((x+1)%h,y)
    
    voisins=[voisin_haut,voisin_bas,voisin_droite,voisin_gauche]

    return voisins



def cases_dispos(grille,abscisse,ordonnee,valeur):
    '''
    Renvoie une liste constituée des coordonnées des voisins de la case dont la valeur correspondant au poisson cherché
    :param grille: (list) une liste de liste 
    :param abscisse: (int) abscisse du point autour duquel on cherche les cellules
    :param ordonnee: (int) ordonnee du point autour duquel on cherche les cellules
    :return: (list) liste de coordonnées
    
    >>> grille = [[2,0,0],[1,2,0],[0,2,0]]
    >>> cases_dispos(grille,1,2,2)
    [(1, 1)]
    '''
    x,y=abscisse,ordonnee
    voisins=voisins_cellule(grille,x,y)
    cases=[]
    
    for i,j in voisins:
        if grille[i][j]==valeur:
            cases.append((i,j))
    
    return cases


import doctest
doctest.testmod(verbose=False)
