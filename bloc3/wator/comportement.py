from random import randint,choice
from preparation import *


def gestation(grille,gestation_init_thon,gestation_init_req):
    '''
    Crée une grille contenant les valeurs de gestation des différentes espèces selon la mer en paramètre
    :param grille: (list) grille sous forme d'une liste de listes
    :param gestation_init_thon: (int) valeur initiale de gestation d'un thon
    :param gestation_init_req: (int) valeur initiale de gestation d'un requin
    :return: (list) grille sous forme d'une liste de listes
    Exemple:
    
    >>> gestation([[1,0,2],[0,0,1],[2,2,1]],2,6)
    [[6, 0, 2], [0, 0, 6], [2, 2, 6]]
    '''
    
    ges=creer_grille(largeur_grille(grille),hauteur_grille(grille))
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):
            
            if grille[i][j]==2:
                ges[i][j]=gestation_init_thon
                
            elif grille[i][j]==1:
                ges[i][j]=gestation_init_req
                
            elif grille[i][j]==0:
                ges[i][j]=0
    return ges



def energ(grille,energie_init):
    '''
    Crée une grille contenant les valeurs d'énergie des différentes espèces selon la mer en paramètre
    :param grille: (list) grille sous forme d'une liste de listes
    :param energie_init: (int) valeur initiale d'énergie d'un requin
    :return: (list) grille sous forme d'une liste de listes
    Exemple:
    
    >>> energ([[1, 0, 2], [0, 0, 1], [2, 2, 1]],3)
    [[3, 0, 0], [0, 0, 3], [0, 0, 3]]
    '''
    
    energie=creer_grille(largeur_grille(grille),hauteur_grille(grille))
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):
            
            if grille[i][j]==1:
                energie[i][j]=energie_init
            else:
                energie[i][j]=0

    return energie


def thon_seul(grille,ges,gestation_init_thon):
    '''
    Permet de déplacer un thon dans la mer/grille en faisant évoluer sa gestation
    
    :param grille: (list) liste de liste
    :param ges: (list) liste de liste contenant les valeurs de gestation des thons et des requins
    :param gestation_init_thon: (int) valeur initiale de gestation d'un thon
    :return: deux listes de listes
    '''
    
    x=randint(0,largeur_grille(grille)-1)
    y=randint(0,hauteur_grille(grille)-1)
        
    # case avec un thon 
    if grille[x][y]==2:
        
        # choix dans les cases libres autour
        if len(cases_dispos(grille,x,y,0))>0:
        
            (a,b)=choice(cases_dispos(grille,x,y,0))
            grille[a][b]=2
                
            if ges[x][y]>0:
                grille[x][y]=0
                ges[a][b]=ges[x][y]-1
                ges[x][y]=0
                            
            else:
                grille[x][y]=2
                ges[a][b]=gestation_init_thon
                ges[x][y]=gestation_init_thon
            
        # cases autour occupées
        else:
            if ges[x][y]>0:
                ges[x][y]-=1
            
            else:
                ges[x][y]=gestation_init_thon
  
            
    return grille,ges






def requin_seul(grille,ges,gestation_init_req,energie,energie_init):
    '''
    Permet de déplacer un requin dans la mer/grille en faisant évoluer sa gestation et son énergie
    
    :param grille: (list) liste de liste
    :param ges: (list) liste de liste contenant les valeurs de gestation des thons et des requins
    :param gestation_init_req: (int) valeur initiale de gestation d'un requin
    :param energie: (list) liste de liste contenant les valeurs d'énergie des requins
    :param energie_init: (int) valeur initiale d'énergie d'un requin
    :return: trois listes de listes
    '''
    
    x=randint(0,largeur_grille(grille)-1)
    y=randint(0,hauteur_grille(grille)-1)
    
    energie[x][y]-=1
    
    # case un requin plein d'énergie ^^
    if grille[x][y]==1 and energie[x][y]!=0:
        
        # si des cases avec thon sont disponibles
        if len(cases_dispos(grille,x,y,2))>0:
            
            # choix dans les cases autour contenant un thon autour
            (a,b)=choice(cases_dispos(grille,x,y,2))
            
            grille[a][b]=1
            
            if ges[x][y]>0:
                grille[x][y]=0
                ges[a][b]=ges[x][y]-1
                ges[x][y]=0
                energie[a][b]=energie_init
                energie[x][y]=0
                            
            else:
                grille[x][y]=1
                ges[a][b]=gestation_init_req
                ges[x][y]=gestation_init_req
                energie[a][b]=energie_init
                energie[x][y]=energie_init
        
        # si des cases avec thon ne sont disponibles
        else:
            
            # choix dans les cases libres autour
            if len(cases_dispos(grille,x,y,0))>0:
        
                (a,b)=choice(cases_dispos(grille,x,y,0))
                
                grille[a][b]=1
                
                if ges[x][y]>0:
                    grille[x][y]=0
                    ges[a][b]=ges[x][y]-1
                    ges[x][y]=0
                    energie[a][b]=energie[x][y]
                    energie[x][y]=0
                                
                else:
                    grille[x][y]=1
                    ges[a][b]=gestation_init_req
                    ges[x][y]=gestation_init_req
                    energie[a][b]=energie[x][y]
                    energie[x][y]=energie_init

    # case un requin sans énergie            
    elif energie[x][y]==0 and grille[x][y]==1:
        grille[x][y]=0
        ges[x][y]=0
    
    return grille,ges,energie